package data;

import java.util.Date;

public class ClientItem {
    private int item_id;
    private Date insertionDate;
    private String name;
    private String category;
    private String country;
    private String photograph;
    private Double price;

    private ClientUser user;

    public ClientItem(int item_id, Date insertionDate, String name, String category, String country, String photograph, Double price, ClientUser user) {
        this.item_id = item_id;
        this.insertionDate = insertionDate;
        this.name = name;
        this.category = category;
        this.country = country;
        this.photograph = photograph;
        this.price = price;
        this.user = user;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public Date getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(Date insertionDate) {
        this.insertionDate = insertionDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhotograph() {
        return photograph;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ClientUser getUser() {
        return user;
    }

    public void setUser(ClientUser user) {
        this.user = user;
    }
}
