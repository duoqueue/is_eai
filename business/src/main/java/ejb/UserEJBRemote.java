package ejb;

import data.ClientItem;
import data.ClientUser;

import java.util.Date;
import java.util.List;

public interface UserEJBRemote {
    boolean registerUser(String email, String password, String firstName, String lastName, String country);

    String loginUser(String email, String password);

    ClientUser userInfo(String email);

    boolean updateInfo(String email, String firstName, String lastName, String country);

    boolean deleteUser(String email);

    boolean addItem(String email, String name, String category, String country, String photograph, String price);

    List<ClientItem> allItems();

    ClientItem itemDetails(int item_id);

    List<ClientItem> searchItems(String email, String search, String Category, Double minPrice, Double maxPrice, Date data, boolean countryBool, boolean myItemsBool, boolean sortOrder, String sortBy);

    boolean deleteItem(int item_id);

    boolean updateItem(int id, String name, Double price, String country, String category, String photograph);

    boolean matchPw(String email, String password);

    boolean updatePw(String email, String password);
}
