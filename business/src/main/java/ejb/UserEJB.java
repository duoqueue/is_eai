package ejb;

//import com.sun.security.ntlm.Client;

import data.ClientItem;
import data.ClientUser;
import data.Item;
import data.User;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

import javax.ejb.Stateless;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Stateless
public class UserEJB implements UserEJBRemote {
    @PersistenceContext(name = "User")
    EntityManager em;
    final static Logger logger = Logger.getLogger(UserEJB.class);

    @Override
    public boolean registerUser(String email, String password, String firstName, String lastName, String country) {
        User novo = new User();
        novo.setEmail(email);
        novo.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        novo.setfName(firstName);
        novo.setlName(lastName);
        novo.setCountry(country);
        try {
            em.persist(novo);
            em.flush();
            logger.debug("Added a user: " + novo.getEmail());
            return true;
        } catch (PersistenceException ex) {
            logger.error("Error adding user! User already exists");
            return false;
        } catch (IllegalArgumentException ex) {
            logger.error("Error adding user!");
            return false;
        }
    }

    public String loginUser(String email, String password) {
        Query q = null;
        q = em.createQuery("select u.password from User u where u.email = :email");
        q.setParameter("email", email);
        try {
            List<String> result = q.getResultList();
            logger.debug("Login Query looking for user : " + email);
            if (result.isEmpty()) {
                logger.debug("User not found");
                return "User not found!";
            } else {
                logger.debug("User found");
                if (BCrypt.checkpw(password, result.get(0))) {
                    return "T";
                } else {
                    return "Wrong password, please try again !";
                }
            }
        } catch (PersistenceException ex) {
            logger.error("Query timeout looking for user!");
            return "Something went wrong!";
        }
    }

    public boolean matchPw(String email, String password) {
        Query q = em.createQuery("select u.password from User u where u.email = :email");
        q.setParameter("email", email);
        try {
            logger.debug("Looking for user : " + email);
            List<String> result = q.getResultList();
            if (result.isEmpty()) {
                logger.debug("User not found");
                return false;
            } else {
                logger.debug("User found");
                return BCrypt.checkpw(password, result.get(0));
            }
        } catch (PersistenceException ex) {
            logger.error("Query timeout looking for user!");
            return false;
        }
    }

    public boolean updatePw(String email, String password) {
        try {
            logger.debug("Looking for user: " + email);
            User user = em.find(User.class, email);
            user.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
            em.flush();
            logger.debug("Password Changed");
            return true;
        } catch (IllegalArgumentException ex) {
            logger.error("User " + email + " not found!");
            return false;
        } catch (PersistenceException ex) {
            logger.error("SQL exception");
            return false;
        }
    }

    public ClientUser userInfo(String email) {
        try {
            logger.debug("Looking for user: " + email);
            User user = em.find(User.class, email);
            ClientUser clientUser = new ClientUser(user.getEmail(), user.getCountry(), user.getfName(), user.getlName());
            return clientUser;
        } catch (IllegalArgumentException ex) {
            logger.error("User " + email + " not found!");
            return null;
        }
    }

    public boolean updateInfo(String email, String firstName, String lastName, String country) {
        try {
            logger.debug("Looking for user: " + email);
            User user = em.find(User.class, email);
            user.setfName(firstName);
            user.setlName(lastName);
            user.setCountry(country);
            em.flush();
            logger.debug("Info Updated");
            return true;
        } catch (IllegalArgumentException ex) {
            logger.error("User " + email + " not found!");
            return false;
        } catch (PersistenceException ex) {
            logger.error("SQL exception");
            return false;
        }
    }

    public boolean deleteUser(String email) {
        try {
            logger.debug("Looking for user: " + email);
            User user = em.find(User.class, email);
            em.remove(user);
            logger.debug("User: " + email + "removed");
            return true;
        } catch (IllegalArgumentException | TransactionRequiredException ex) {
            logger.error("User " + email + " not found! Or transaction required");
            return false;
        }
    }

    public boolean deleteItem(int item_id) {
        try {
            logger.debug("Delete Item: looking for Item: " + item_id);
            Item item = em.find(Item.class, item_id);
            em.remove(item);
            logger.debug("Delete Item: Item: " + item_id + "removed");
            return true;
        } catch (IllegalArgumentException | TransactionRequiredException ex) {
            logger.error("Item: " + item_id + " not found! Or transaction required");
            return false;
        }
    }


    public boolean addItem(String email, String name, String category, String country, String photograph, String price) {
        try {
            User user = em.find(User.class, email);
            Item item = new Item();
            item.setName(name);
            item.setCategory(category);
            item.setCountry(country);
            item.setPhotograph(photograph);
            Timestamp dateTime = new Timestamp(System.currentTimeMillis());
            item.setInsertionDate(dateTime);
            item.setPrice(Double.valueOf(price));
            item.setUser(user);
            em.persist(item);
            em.flush();
            logger.debug("Added a item!");
            return true;
        } catch (EntityExistsException ex) {
            logger.error("Error adding item! Item already exists");
            return false;
        } catch (IllegalArgumentException | PersistenceException ex) {
            logger.error("Error adding Item!");
            return false;
        }
    }


    public List<ClientItem> allItems() {
        try {
            logger.debug("Looking for all items");
            Query q = em.createQuery("from Item i ");
            List<Item> lista = q.getResultList();
            List<ClientItem> clientItems = new ArrayList<>();
            for (int i = 0; i < lista.size(); i++) {
                Item item = lista.get(i);
                ClientItem clientItem = new ClientItem(item.getItem_id(), item.getInsertionDate(), item.getName(), item.getCategory(), item.getCountry(), item.getPhotograph(), item.getPrice(), new ClientUser(item.getUser().getEmail(), item.getUser().getCountry(), item.getUser().getfName(), item.getUser().getlName()));
                clientItems.add(clientItem);
            }
            return clientItems;
        } catch (PersistenceException ex) {
            logger.error("Query timeout looking for items!");
            return null;
        }
    }


    public List<ClientItem> searchItems(String email, String search, String Category, Double minPrice, Double maxPrice, Date data, boolean countryBool, boolean myItemsBool, boolean sortOrder, String sortBy) {
        StringBuilder queryString = new StringBuilder();
        List<ClientItem> clientItems = new ArrayList<>();
        User user = em.find(User.class, email);
        queryString.append("from Item i where i.price >= :minPrice and i.price<= :maxPrice");
        if (search != null && !search.isEmpty()) {
            queryString.append(" and i.name like :search");
        }
        if(data !=null){
            queryString.append(" and i.insertionDate >= :date");
        }
        if (!Category.equals("all"))
            queryString.append(" and i.category = :category");
        if (countryBool)
            queryString.append(" and i.country = :country");
        if (myItemsBool)
            queryString.append(" and i.user = :user");
        if (sortBy.equals("dateInsertion")) {
            queryString.append(" order by i.insertionDate");
        } else if (sortBy.equals("name")) {
            queryString.append(" order by i.name");
        } else {
            queryString.append(" order by i.price");
        }
        if (sortOrder)
            queryString.append(" ASC");
        else
            queryString.append(" DESC");
        logger.debug("Query: " + queryString.toString());
        try {
            Query q = em.createQuery(queryString.toString());
            q.setParameter("minPrice", minPrice);
            q.setParameter("maxPrice", maxPrice);
            if (search != null && !search.isEmpty()) {
                q.setParameter("search", "%" + search + "%");
            }
            if(data !=null){
                q.setParameter("date", data);
            }
            if (!Category.equals("all"))
                q.setParameter("category", Category);
            if (countryBool) {
                q.setParameter("country", user.getCountry());
            }
            if (myItemsBool)
                q.setParameter("user", user);
            List<Item> lista = q.getResultList();
            for (int i = 0; i < lista.size(); i++) {
                Item item = lista.get(i);
                ClientItem clientItem = new ClientItem(item.getItem_id(), item.getInsertionDate(), item.getName(), item.getCategory(), item.getCountry(), item.getPhotograph(), item.getPrice(), new ClientUser(item.getUser().getEmail(), item.getUser().getCountry(), item.getUser().getfName(), item.getUser().getlName()));
                clientItems.add(clientItem);
            }
        } catch (PersistenceException ex) {
            logger.error("Query timeout searching items!");
        }
        return clientItems;
    }

    public ClientItem itemDetails(int item_id) {
        try {
            logger.debug("Looking for item: " + item_id);
            Item item = em.find(Item.class, item_id);
            ClientItem clientItem = new ClientItem(item.getItem_id(), item.getInsertionDate(), item.getName(), item.getCategory(), item.getCountry(), item.getPhotograph(), item.getPrice(), new ClientUser(item.getUser().getEmail(), item.getUser().getCountry(), item.getUser().getfName(), item.getUser().getlName()));
            return clientItem;
        } catch (IllegalArgumentException ex) {
            logger.error("Item " + item_id + " not found!");
            return null;
        }
    }

    public boolean updateItem(int id, String name, Double price, String country, String category, String photograph) {
        try {
            logger.debug("Looking for item: " + id);
            Item item = em.find(Item.class, id);
            item.setName(name);
            item.setPrice(price);
            item.setCountry(country);
            item.setCategory(category);
            item.setPhotograph(photograph);
            em.flush();
            logger.debug("Item updated");
            return true;
        } catch (IllegalArgumentException ex) {
            logger.error("Item " + id + " not found!");
            return false;
        } catch (PersistenceException ex) {
            logger.error("SQL exception");
            return false;
        }
    }
}
