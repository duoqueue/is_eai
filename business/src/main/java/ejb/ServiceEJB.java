package ejb;

import data.Item;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Properties;

@Singleton
public class ServiceEJB {
    @PersistenceContext(name = "User")
    EntityManager em;

    @Schedule(hour = "*", minute = "*/30", second = "0", persistent = false)
    public void sendEmail() {
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.put("mail.smtp.host", "smtp.mailtrap.io");
        prop.put("mail.smtp.port", "25");
        prop.put("mail.smtp.ssl.trust", "smtp.mailtrap.io");

        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("c5569f8cc94678", "bf26d364ad8666");
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("from@gmail.com"));
            Query q = em.createQuery("select u.email from User u");
            List<String> result = q.getResultList();
            String addresses = "";
            for (int i = 0; i < result.size(); i++) {
                if (i == result.size() - 1)
                    addresses = addresses.concat(result.get(i));
                else
                    addresses = addresses.concat(result.get(i) + ",");
            }
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(addresses));
            message.setSubject("Mail Subject");
            q = em.createQuery("select i from Item i order by i.insertionDate DESC");
            q.setMaxResults(3);
            List<Item> items = q.getResultList();
            String msg = "<h1>Catalog</h1><h2>Check our 3 newest products!</h2><table><tr>";
            for (int i = 0; i < items.size(); i++) {
                msg = msg.concat("<td>Name: " + items.get(i).getName() + "<br>Category: " + items.get(i).getCategory() + "<br>Price: " + items.get(i).getPrice() + "<br><img height=\"200px\" width=\"200px\" src=\"" + items.get(i).getPhotograph() + "\"></td>");
            }
            msg = msg.concat("</tr></table>");
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            mimeBodyPart.setContent(msg, "text/html");

            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimeBodyPart);

            message.setContent(multipart);

            Transport.send(message);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}

