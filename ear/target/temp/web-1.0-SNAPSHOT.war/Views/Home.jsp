<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 29/10/19
  Time: 18:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<script>
    function updateMinInput(val) {
        document.getElementById('MinInput').value = val;
        document.getElementById('minRange').value = val;
    }

    function updateMaxInput(val) {
        document.getElementById('MaxInput').value = val;
        document.getElementById('maxRange').value = val;
    }
</script>
<head>
    <title>MyBay-Home</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<div style="display: flex ; justify-content: center">
    <a style="margin-left: 2.5em" href="additem" target="_self">Add Item</a>
    <a style="margin-left: 2.5em" href="editAccount" target="_self">Edit Personal Information</a>
    <a style="margin-left: 2.5em" href="logout" target="_self">Logout</a>
</div>
<hr>
<div style="height: 100%">
    <div style="display: inline-block; vertical-align: top;height: 100%; width: 25%">
        FILTERS :
        <form action="home" method="post" style="">
            Search: <input type="text" name="search" id="search" value="${param.search}"><br>
            Category: <select name="category">
            <option value="all" selected>all</option>
            <option value="Computers">Computers</option>
            <option value="Tablets">Tablets</option>
            <option value="Smartphones">Smartphones</option>
            <option value="TV">TV</option>
            <option value="Software">Software</option>
        </select><br>
            Min Price <input type="range" id="minRange" min="0" max="1000" value="0" class="slider"
                             onchange="updateMinInput(this.value);">
            <input type="text" name="minPrice" id="MinInput" value="0" onkeyup="updateMinInput(this.value)"
                   required><br>
            Max Price <input type="range" id="maxRange" min="0" max="1000" value="1000" class="slider"
                             onchange="updateMaxInput(this.value);">
            <input type="text" name="maxPrice" id="MaxInput" value="1000" onkeyup="updateMaxInput(this.value)" required><br>
            Date:<input type="datetime-local" name="data"><br>
            <input type="checkbox" name="myItemsCheck" value="True"> Only my items<br>
            <input type="hidden" name="myItemsCheck" value="False">
            <input type="checkbox" name="countryCheck" value="True"> Only items from my country<br>
            <input type="hidden" name="countryCheck" value="False">
            <input type="checkbox" name="sortOrder" value="True"> Ascending order <br>
            <input type="hidden" name="sortOrder" value="False">
            Sort By: <select name="sortOption">
            <option value="dateInsertion">Date of insertion</option>
            <option value="name">Name</option>
            <option value="price">Price</option>
        </select><br>
            <input type="submit" value="Search">
        </form>
    </div>

    <div style="display: inline-block; vertical-align: top;height: 100%; width: 74%; overflow: auto">
        <table align="center">
            <script>var i = 0;</script>
            <c:forEach items="${allItems}" var="item">
                <script>if (i % 2 == 0) {
                    document.write("<tr>")
                }</script>
                <td>Name: <c:out value="${item.name}"/><br>
                    Category: <c:out value="${item.category}"/><br>
                    Country: <c:out value="${item.country}"/><br>
                    Price: <c:out value="${item.price}"/>
                </td>
                <td><a href="itemDetails?id=${item.item_id}"><img height="200px" width="200px"
                                                                  src="<c:out value="${item.photograph}"/>"/></a></td>
                <script>if (i + 1 % 2 == 0) {
                    document.write("</tr>")
                }</script>
                <script>i = i + 1;</script>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>