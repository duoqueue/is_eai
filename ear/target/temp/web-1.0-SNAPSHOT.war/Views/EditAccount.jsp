<%--
  Created by IntelliJ IDEA.
  User: migue
  Date: 02/11/2019
  Time: 13:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Personal Information</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<div style="display: flex ; justify-content: right">
    <a style="margin-left: 2.5em" href="logout" target="_self">Logout</a>
</div>
<hr>
<h2>Edit Personal Information</h2>
<form action="editAccount" method="post">
    First Name: <input type="text" name="firstname" id="firstname"
                       value="${user.fName}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.firstname}</span><br>
    Last Name: <input type="text" name="lastname" id="lastname"
                      value="${user.lName}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.lastname}</span><br>
    <select name="country">
        <option value="Portugal"  ${user.country == 'Portugal' ? 'selected' : ''}>Portugal</option>
        <option value="Spain"     ${user.country == 'Spain' ? 'selected' : ''} >Spain</option>
        <option value="France"    ${user.country == 'France' ? 'selected' : ''} >France</option>
        <option value="Germany"   ${user.country == 'Germany' ? 'selected' : ''}>Germany</option>
        <option value="Italy"     ${user.country == 'Italy' ? 'selected' : ''}>Italy</option>
    </select><br><br>
    <input type="submit" name="action" value="Save">
</form>
<form action="editAccount" method="post">
    Old Password: <input type="password" name="old_password" id="old_password"
                         value="${param.old_password}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.old_password}</span><br>
    New Password: <input type="password" name="new_password" id="new_password"
                         value="${param.new_password}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.new_password}</span><br>
    Re-NewPassword: <input type="password" name="re_password" id="re_password"
                           value="${param.re_password}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.re_password}</span><br>
    <input type="submit" name="action" value="Change Password">
</form>
<form action="editAccount" method="post">
    <input type="submit" name="action" value="Delete"/>
</form>
</body>
</html>
