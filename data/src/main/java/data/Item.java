package data;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Entity
@Table(name = "item")
public class Item implements Serializable {
    private static final long serialVersionUID = -6576815419613791621L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int item_id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "insertionDate", nullable = false)
    private Date insertionDate;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "category", nullable = false, length=500)
    private String category;

    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "photograph", nullable = false)
    private String photograph;

    @Column(name = "price", nullable = false)
    private Double price;

    @ManyToOne
    private User user;

    public Item() {
        super();
    }

    public Item(Date insertionDate, String name, String category, String country, String photograph, User user, Double price) {
        super();
        this.insertionDate = insertionDate;
        this.name = name;
        this.category = category;
        this.country = country;
        this.photograph = photograph;
        this.user = user;
        this.price = price;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public Date getInsertionDate() {
        return insertionDate;
    }

    public void setInsertionDate(Date insertionDate) {
        this.insertionDate = insertionDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhotograph() {
        return photograph;
    }

    public void setPhotograph(String photograph) {
        this.photograph = photograph;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
