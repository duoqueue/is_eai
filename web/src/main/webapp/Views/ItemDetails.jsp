<%--
  Created by IntelliJ IDEA.
  User: migue
  Date: 03/11/2019
  Time: 18:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Item Details</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<div style="display: flex ; justify-content: right">
    <a style="margin-left: 2.5em" href="logout" target="_self">Logout</a>
</div>
<hr>
<div>
    Name: ${item.name} <br>
    Category: ${item.category} <br>
    Insertion Date: ${item.insertionDate}<br>
    Country:${item.country}
    Price: ${item.price}<br>
    <img height="300px" width="300px" src="${item.photograph}"/>
</div>
<div style="display: ${access}" id="special">
    <form action="itemDetails" method="post">
        <input type="hidden" name="id_item" value="${item.item_id}">
        <input type="submit" name="action" value="Edit"/>
    </form>
    <form action="itemDetails" onSubmit="if(!confirm('Do you really want to delete it?')){return false;}" method="post">
        <input type="hidden" name="id_item" value="${item.item_id}">
        <input type="submit" name="action" value="Delete"/>
    </form>
</div>
</body>
</html>
