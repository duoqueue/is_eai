<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 28/10/19
  Time: 12:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>MyBay-Regist</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<hr>
<h2>Create account</h2>
<form action="register" method="post" id="register">
    First Name: <input type="text" name="firstname" id="firstname"
                       value="${param.firstname}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.firstname}</span><br>
    Last Name: <input type="text" name="lastname" id="lastname"
                      value="${param.lastname}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.lastname}</span><br>
    Email: <input type="email" name="email" id="email"
                  value="${param.email}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.email}</span><br>
    Password: <input type="password" name="password" id="password"
                     value="${param.password}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.password}</span><br>
    Re-Password: <input type="password" name="re_password" id="re_password"
                        value="${param.re_password}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.re_password}</span><br>
    <select name="country">
        <option value="Portugal">Portugal</option>
        <option value="Spain">Spain</option>
        <option value="France">France</option>
        <option value="Germany">Germany</option>
        <option value="Italy">Italy</option>
    </select><br>
    <input type="submit" value="Register">
</form>
</body>
</html>

