<%--
  Created by IntelliJ IDEA.
  User: diogo
  Date: 02/11/19
  Time: 14:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Item</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<div style="display: flex ; justify-content: right">
    <a style="margin-left: 2.5em" href="logout" target="_self">Logout</a>
</div>
<hr>
<h2>Add a new item</h2>
<form action="additem" method="post">
    Name: <input type="text" name="name" id="name" value="${param.name}" ${not empty messages.succes ? 'disabled' : ''}
                 required><br>
    <span class="error" style="color:red">${messages.name}</span><br>
    Price: <input type="number" step="0.01" min="0" name="price" id="price"
                  value="${param.price}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.price}</span><br>
    Country: <select name="country">
    <option value="Portugal">Portugal</option>
    <option value="Spain">Spain</option>
    <option value="France">France</option>
    <option value="Germany">Germany</option>
    <option value="Italy">Italy</option>
</select><br>
    Category: <select name="category">
    <option value="Computers">Computers</option>
    <option value="Tablets">Tablets</option>
    <option value="Smartphones">Smartphones</option>
    <option value="TV">TV</option>
    <option value="Software">Software</option>
</select><br>
    Photograph-URL: <input type="url" name="photograph" id="photograph"
                           value="${param.photograph}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.photograph}</span><br>
    <input type="submit" value="Add Item">
</form>
</body>
</html>
