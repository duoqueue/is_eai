package servlet;

import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/editAccount")
public class EditAccountServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(EditAccountServlet.class);

    public EditAccountServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("HTTP get received!");
        RequestDispatcher editPage = request.getRequestDispatcher("Views/EditAccount.jsp");
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            request.setAttribute("user", userEJB.userInfo(session.getAttribute("email").toString()));
            editPage.forward(request, response);
        } else {
            logger.info("Invalid Session!");
            response.sendRedirect("login");
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("HTTP Post received!");
        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            String email = session.getAttribute("email").toString();
            if (req.getParameter("action").equals("Save")) {
                logger.info("Action: Account info changes");
                Map<String, String> messages = new HashMap<String, String>();
                req.setAttribute("messages", messages);

                String firstname = req.getParameter("firstname");
                String lastname = req.getParameter("lastname");
                String country = req.getParameter("country");

                if (firstname == null || firstname.trim().isEmpty()) {
                    messages.put("firstname", "Please enter this field !");
                }
                if (lastname == null || lastname.trim().isEmpty()) {
                    messages.put("lastname", "Please enter this field !");
                }

                if (!messages.isEmpty()) {
                    req.getRequestDispatcher("Views/EditAccount.jsp").forward(req, resp);
                } else {
                    if (userEJB.updateInfo(email, firstname, lastname, country)) {
                        resp.sendRedirect("editAccount");
                    } else {
                        messages.put("firstname", "Something went wrong!");
                        req.getRequestDispatcher("Views/EditAccount.jsp").forward(req, resp);
                    }
                }
            } else if (req.getParameter("action").equals("Change Password")) {
                logger.info("Action: Password Change");
                Map<String, String> messages = new HashMap<String, String>();
                req.setAttribute("messages", messages);

                String old_password = req.getParameter("old_password");
                String new_password = req.getParameter("new_password");
                String re_password = req.getParameter("re_password");

                if (old_password == null || old_password.trim().isEmpty()) {
                    messages.put("old_password", "Please enter this field !");
                } else {
                    if (!userEJB.matchPw((email), old_password)) {
                        messages.put("old_password", "Wrong password, please try again!");
                    }
                }
                if (new_password == null || new_password.trim().isEmpty()) {
                    messages.put("new_password", "Please enter this field !");
                }
                if (re_password == null || re_password.trim().isEmpty()) {
                    messages.put("re_password", "Please enter this field !");
                }
                if (!new_password.equals(re_password)) {
                    messages.put("re_password", "Passwords doesn't match, try again!");
                }

                if (!messages.isEmpty()) {
                    req.getRequestDispatcher("Views/EditAccount.jsp").forward(req, resp);
                } else {
                    if (userEJB.updatePw(email, new_password)) {
                        resp.sendRedirect("editAccount");
                    } else {
                        messages.put("old_password", "Something went wrong!");
                        req.getRequestDispatcher("Views/EditAccount.jsp").forward(req, resp);
                    }
                }
            } else if (req.getParameter("action").equals("Delete")) {
                logger.info("Action: Delete Account");
                if (userEJB.deleteUser(email)) {
                    resp.sendRedirect("logout");
                }
            }
        } else {
            logger.info("Invalid Session!");
            resp.sendRedirect("login");
        }
    }


}
