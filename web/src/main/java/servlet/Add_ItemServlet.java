package servlet;

import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Add_ItemServlet")
public class Add_ItemServlet extends HttpServlet {

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(Add_ItemServlet.class);

    private static final long serialVersionUID = 1L;

    public Add_ItemServlet() {
        super();
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Http post received!");
        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            Map<String, String> messages = new HashMap<String, String>();
            req.setAttribute("messages", messages);
            String name = req.getParameter("name");
            String country = req.getParameter("country");
            String category = req.getParameter("category");
            String photograph = req.getParameter("photograph");
            String price = req.getParameter("price");

            if (name == null || name.trim().isEmpty()) {
                messages.put("name", "Please enter this field !");
            }
            if (photograph == null || photograph.trim().isEmpty()) {
                messages.put("photograph", "Please enter this field !");
            }
            if (price == null || price.trim().isEmpty()) {
                messages.put("price", "Please enter this field !");
            }
            if (!messages.isEmpty()) {
                req.getRequestDispatcher("Views/Add_Item.jsp").forward(req, resp);
            } else {
                logger.debug("Add item called");
                if (!userEJB.addItem((String) req.getSession(false).getAttribute("email"), name, category, country, photograph, price)) {
                    messages.put("name", "Something went wrong adding item!");
                    req.getRequestDispatcher("Views/Add_Item.jsp").forward(req, resp);
                }
                resp.sendRedirect("home");
            }
        } else {
            logger.info("Invalid Session!");
            resp.sendRedirect("login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Http get received!");
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            request.getRequestDispatcher("Views/Add_Item.jsp").forward(request, response);
        } else {
            logger.info("Invalid Session!");
            response.sendRedirect("login");
        }
    }
}
