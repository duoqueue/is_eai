package servlet;

import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


@WebServlet("")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(LoginServlet.class);

    public LoginServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("HTTP get received!");
        RequestDispatcher loginPage = request.getRequestDispatcher("Views/Login.jsp");
        HttpSession session = request.getSession();
        session.invalidate();
        loginPage.include(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("HTTP post received!");
        Map<String, String> messages = new HashMap<String, String>();
        req.setAttribute("messages", messages);

        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (email == null || email.trim().isEmpty()) {
            messages.put("email", "Please enter this field !");
        } else {
            String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
            Pattern pattern = Pattern.compile(regex);
            if (!pattern.matcher(email).matches()) {
                messages.put("email", "Please enter a valid email!");
            }
        }
        if (password == null || password.trim().isEmpty()) {
            messages.put("password", "Please enter this field !");
        }

        if (!messages.isEmpty()) {
            req.getRequestDispatcher("Views/Login.jsp").forward(req, resp);
        } else {
            String r = userEJB.loginUser(email, password);
            if (r.equals("T")) { //user logged in
                HttpSession session = req.getSession(true);
                session.setAttribute("email", email);
                session.setMaxInactiveInterval(1800);
                resp.sendRedirect("home");
            } else {
                messages.put("password", r);
                req.getRequestDispatcher("Views/Login.jsp").forward(req, resp);
            }
        }
    }

}
