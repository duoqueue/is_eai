package servlet;

import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(RegisterServlet.class);
    public RegisterServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("HTTP get received!");
        RequestDispatcher registerPage = req.getRequestDispatcher("Views/Register.jsp");
        HttpSession session = req.getSession();
        session.invalidate();
        registerPage.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("HTTP post received!");
        Map<String, String> messages = new HashMap<String, String>();
        req.setAttribute("messages", messages);

        String firstname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String re_password = req.getParameter("re_password");
        String country = req.getParameter("country");

        if (firstname == null || firstname.trim().isEmpty()) {
            messages.put("firstname", "Please enter this field !");

        }
        if (lastname == null || lastname.trim().isEmpty()) {
            messages.put("lastname", "Please enter this field !");
        }
        if (email == null || email.trim().isEmpty()) {
            messages.put("email", "Please enter this field !");
        } else {
            String regex = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
            Pattern pattern = Pattern.compile(regex);
            if (!pattern.matcher(email).matches()) {
                messages.put("email", "Please enter a valid email!");
            }
        }
        if (password == null || password.trim().isEmpty()) {
            messages.put("password", "Please enter this field !");
        }
        if (re_password == null || re_password.trim().isEmpty()) {
            messages.put("re_password", "Please enter this field !");
        }
        if (!password.equals(re_password)) {
            messages.put("re_password", "Passwords doesn't match, try again!");
        }


        if (!messages.isEmpty()) {
            req.getRequestDispatcher("Views/Register.jsp").forward(req, resp);
        } else {
            if(!userEJB.registerUser(email, password, firstname, lastname, country)){
                messages.put("email", "Email already exists!");
                req.getRequestDispatcher("Views/Register.jsp").forward(req, resp);
            }
            req.getRequestDispatcher("Views/Login.jsp").forward(req, resp);
        }
    }
}
