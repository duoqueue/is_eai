package servlet;

import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/editItem")
public class EditItemServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(EditItemServlet.class);

    public EditItemServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("HTTP get received!");
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            request.setAttribute("item", userEJB.itemDetails(Integer.parseInt(request.getParameter("id"))));
            request.getRequestDispatcher("Views/EditItem.jsp").forward(request, response);
        } else {
            logger.info("Invalid Session!");
            response.sendRedirect("login");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("HTTP post received!");
        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            Map<String, String> messages = new HashMap<String, String>();
            req.setAttribute("messages", messages);
            String name = req.getParameter("name");
            String country = req.getParameter("country");
            String category = req.getParameter("category");
            String photograph = req.getParameter("photograph");
            String price = req.getParameter("price");
            if (name == null || name.trim().isEmpty()) {
                messages.put("name", "Please enter this field !");
            }
            if (photograph == null || photograph.trim().isEmpty()) {
                messages.put("photograph", "Please enter this field !");
            }
            if (price == null || price.trim().isEmpty()) {
                messages.put("price", "Please enter this field !");
            }
            if (!messages.isEmpty()) {
                req.getRequestDispatcher("Views/Add_Item.jsp").forward(req, resp);
            } else {
                if (!userEJB.updateItem(Integer.parseInt(req.getParameter("item_id")), name, Double.valueOf(price), country, category, photograph)) {
                    messages.put("name", "Something went wrong!");
                    req.getRequestDispatcher("Views/EditAccount.jsp").forward(req, resp);
                }
                resp.sendRedirect("home");
            }
        } else {
            logger.info("Invalid Session!");
            resp.sendRedirect("login");
        }
    }


}
