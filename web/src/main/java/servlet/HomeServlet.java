package servlet;

import data.ClientItem;
import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@WebServlet("/home")
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(HomeServlet.class);

    public HomeServlet() {
        super();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("HTTP get received!");
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            String email = (String) session.getAttribute("email");
            List<ClientItem> allItems = userEJB.allItems();
            request.setAttribute("allItems", allItems);
            request.getRequestDispatcher("Views/Home.jsp").forward(request, response);
        } else {
            logger.info("Invalid Session!");
            response.sendRedirect("login");
        }
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("HTTP post received!");
        HttpSession session = req.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            String email = (String) session.getAttribute("email");
            String search = req.getParameter("search");
            String Category = req.getParameter("category");
            Double minPrice = Double.parseDouble(req.getParameter("minPrice"));
            Double maxPrice = Double.parseDouble(req.getParameter("maxPrice"));
            logger.info(req.getParameter("data"));
            Date data;
            try {
               data =  new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(req.getParameter("data").replace("T"," "));
                logger.info(data.toString());
            } catch (ParseException e) {
                logger.info(e.getMessage());
                data=null;
                e.printStackTrace();
            }
            boolean myItemsBool = Boolean.parseBoolean(req.getParameter("myItemsCheck"));
            boolean countryBool = Boolean.parseBoolean(req.getParameter("countryCheck"));
            boolean sortOrder = Boolean.parseBoolean(req.getParameter("sortOrder"));
            String sortBy = req.getParameter("sortOption");
            List<ClientItem> filteredList = userEJB.searchItems(email, search, Category, minPrice, maxPrice, data,countryBool, myItemsBool, sortOrder, sortBy);
            System.out.println(sortOrder);
            req.setAttribute("allItems", filteredList);
            req.getRequestDispatcher("Views/Home.jsp").forward(req, resp);
        } else {
            logger.info("Invalid Session!");
            resp.sendRedirect("login");
        }
    }
}
