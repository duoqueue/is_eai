package servlet;

import data.ClientItem;
import ejb.UserEJBRemote;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

public class ItemDetailsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @EJB
    UserEJBRemote userEJB;
    final static Logger logger = Logger.getLogger(ItemDetailsServlet.class);

    public ItemDetailsServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("HTTP get received!");
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            ClientItem chosenItem = userEJB.itemDetails(Integer.parseInt(request.getParameter("id")));
            if (chosenItem == null) {
                response.sendRedirect("home");
            }
            request.setAttribute("item", chosenItem);
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
            if (chosenItem.getUser().getEmail().equals(session.getAttribute("email"))) {
                request.setAttribute("access", "flex");
            } else {
                request.setAttribute("access", "none");
            }
            request.getRequestDispatcher("Views/ItemDetails.jsp").include(request, response);
            out.close();
        } else {
            logger.info("Invalid Session!");
            response.sendRedirect("login");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("HTTP get received!");
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute("email") != null) {
            if (request.getParameter("action").equals("Delete")) {
                userEJB.deleteItem(Integer.parseInt(request.getParameter("id_item")));
                response.sendRedirect("home");
            } else if (request.getParameter("action").equals("Edit")) {
                response.sendRedirect("editItem?id=" + request.getParameter("id_item"));
            }
        } else {
            logger.info("Invalid Session!");
            response.sendRedirect("login");
        }
    }
}
