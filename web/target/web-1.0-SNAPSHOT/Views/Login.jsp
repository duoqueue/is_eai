<%--
  Created by IntelliJ IDEA.
  User: migue
  Date: 27/10/2019
  Time: 19:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Welcome to MyBay</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<hr>
<h2>Login</h2>
<form action="login" method="post">
    Email: <input type="email" name="email" id="email"
                  value="${param.email}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.email}</span><br>
    Password: <input type="password" name="password" id="password"
                     value="${param.password}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.password}</span><br>
    <input type="submit" value="Login">
</form>
<a href="register" target="_self">Not yet registered ? Click here !</a>
</body>
</html>
