<%--
  Created by IntelliJ IDEA.
  User: migue
  Date: 02/11/2019
  Time: 22:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Item</title>
</head>
<body>
<h1 align="center">MyBay</h1>
<div style="display: flex ; justify-content: right">
    <a style="margin-left: 2.5em" href="logout" target="_self">Logout</a>
</div>
<hr>
<h2>Edit your item</h2>
<form action="editItem" method="post">
    Name: <input type="text" name="name" id="name" value="${item.name}" ${not empty messages.succes ? 'disabled' : ''}
                 required><br>
    <span class="error" style="color:red">${messages.name}</span><br>
    Price: <input type="number" step="0.01" min="0" name="price" id="price"
                  value="${item.price}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.price}</span><br>
    Country: <select name="country">
    <option value="Portugal"  ${item.country == 'Portugal' ? 'selected' : ''}>Portugal</option>
    <option value="Spain"     ${item.country == 'Spain' ? 'selected' : ''} >Spain</option>
    <option value="France"    ${item.country == 'France' ? 'selected' : ''} >France</option>
    <option value="Germany"   ${item.country == 'Germany' ? 'selected' : ''}>Germany</option>
    <option value="Italy"     ${item.country == 'Italy' ? 'selected' : ''}>Italy</option>
</select><br>
    Category: <select name="category">
    <option value="Computers"  ${item.category == 'Computers' ? 'selected' : ''}>Computers</option>
    <option value="Tablets"    ${item.category == 'Tablets' ? 'selected' : ''}>Tablets</option>
    <option value="Smartphones"${item.category == 'Smartphones' ? 'selected' : ''}>Smartphones</option>
    <option value="TV"         ${item.category == 'TV' ? 'selected' : ''}>TV</option>
    <option value="Software"   ${item.category == 'Software' ? 'selected' : ''}>Software</option>
</select><br>
    Photograph-URL: <input type="url" name="photograph" id="photograph"
                           value="${item.photograph}" ${not empty messages.succes ? 'disabled' : ''} required><br>
    <span class="error" style="color:red">${messages.photograph}</span><br>
    <input type="submit" value="Save changes">
    <input type="hidden" name="item_id" id="item_id" value="${item.item_id}">
</form>
</body>
</html>
